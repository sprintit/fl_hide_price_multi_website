# -*- coding: utf-8 -*-
{
    'name': 'Hide Price for Public User in Multi Website',
    'version': '14.0.1.1.0',
    'category': 'Website',
    'summary': 'Hide Product Price from Specific Website for Public User, Hide Product Price for Public User, Website Product Price Invisible for Public User',
    'description': """
        This module allow to hide product price from any specific website for not logged in user or public user in website shop page, 
        Configuration to hide website product price from public user,
        Support for multi-website platform,
        Hide product price and add to cart button in shop page and product page,
        Hide product variant price.
    """,
    'sequence': 1,
    'author': 'Futurelens',
    'website': 'http://thefuturelens.com',
    'depends': ['website_sale', 'sale'],
    'data': [
        'views/website_view.xml',
        'views/website_sale_template.xml'
    ],
    'qweb': [],
    'css': [],
    'js': [],
    'images': [
        'static/description/banner_hide_price_multiwebsite.png',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'OPL-1',
    'price': 15,
    'currency': 'EUR',
}

# -*- coding: utf-8 -*-

from odoo import fields, models


class Website(models.Model):
    _inherit = "website"

    hide_website_price = fields.Boolean("Hide Price for Public User")
